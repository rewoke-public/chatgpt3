import Image from "next/image";
import React, { useState, useEffect } from "react";
import LoadingDots from "../components/LoadingDots";
import languages from "../utils/languages";
import { Toaster, toast } from "react-hot-toast";

const Chat = () => {
  const [loading, setLoading] = useState(false);
  const [language, setLanguage] = useState<string>(languages[0].value);
  const [generatedTranslation, setGeneratedTranslation] = useState<string>("");
  const [text, setText] = useState<string>("");
  const currentModel = "gpt-3.5-turbo";

  const translateText = async () => {
    setGeneratedTranslation("");
    setLoading(true);
    const headers = new Headers();

    headers.append(
      "Authorization",
      `Bearer ${process.env.NEXT_PUBLIC_OPENAI_API_KEY}`
    );
    headers.append("Accept", "application/json");
    headers.append("Content-Type", "application/json");
    const url = "https://api.openai.com/v1/chat/completions";

    return fetch(url, {
      method: "POST",
      body: JSON.stringify({
        model: "gpt-3.5-turbo",
        messages: [
          {
            role: "user",
            content: text,
          },
        ],
      }),
      headers: headers,
    })
      .then((response) => response.json())
      .catch((error) => console.error(error));
  };

  const get_data = async () => {
    setGeneratedTranslation("");
    setLoading(true);
    const transcribed = await translateText();
    setGeneratedTranslation(transcribed.choices[0]?.message.content);
    setLoading(false);
  };

  return (
    <div className="max-w-xl w-full">
      <div className="flex mt-10 items-center space-x-3 ">
        <Image src="/1-black.png" width={30} height={30} alt="1 icon" />
        <p className="text-left font-medium ">Ask Something</p>
      </div>
      <textarea
        className="block p-2.5 my-3 w-full h-[80px] text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-black focus:border-black dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-black dark:focus:border-black"
        placeholder="Write your text here..."
        onChange={(e) => setText(e.target.value)}
      ></textarea>
      {!loading && (
        <button
          className="bg-black rounded-xl text-white font-medium px-4 py-2 sm:mt-1 mt-1 hover:bg-black/80 w-full"
          onClick={get_data}
        >
          Generate A Response &rarr;
        </button>
      )}
      {loading && (
        <button
          className="bg-black rounded-xl text-white font-medium px-4 py-2 sm:mt-10 mt-8 hover:bg-black/80 w-full"
          disabled
        >
          <LoadingDots color="white" style="large" />
        </button>
      )}

      {generatedTranslation && (
        <>
          <div className="p-1">
            <div
              className="p-2.5 w-full  text-gray-900 bg-gray-50 rounded-lg border border-gray-300 cursor-pointer"
              onClick={() => {
                navigator.clipboard.writeText(generatedTranslation);
                toast("Translation copied to clipboard", {
                  icon: "✂️",
                });
              }}
            >
              <p> {generatedTranslation}</p>
            </div>
            <p className="my-1 text-sm text-gray-500 dark:text-gray-300">
              Click on translation to copy on clipboard
            </p>
          </div>
        </>
      )}
    </div>
  );
};

export default Chat;
