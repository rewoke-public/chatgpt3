import type { NextPage } from "next";
import Head from "next/head";
import Image from "next/image";
import React, { useState, useEffect } from "react";
import { Toaster, toast } from "react-hot-toast";
import AudioTranslate from "../components/AudioTranslate";
import Header from "../components/Header";
import TextTranslate from "../components/TextTranslate";
import Chat from "../components/Chat";
const Home: NextPage = () => {
  const [translate, setTranslate] = useState<Boolean>(false);
  const [audio, setAudio] = useState<Boolean>(false);
  const [chatGpt, setChatGpt] = useState<Boolean>(true);
  const set_tab = (tab: string) => {
    if (tab == "translate") {
      setTranslate(true);
      setAudio(false);
      setChatGpt(false);
    } else if (tab == "audio") {
      setTranslate(false);
      setAudio(true);
      setChatGpt(false);
    } else if (tab == "chat_gpt") {
      setTranslate(false);
      setAudio(false);
      setChatGpt(true);
    }
  };
  return (
    <div className="flex max-w-5xl mx-auto flex-col items-center justify-center py-2 min-h-screen">
      <Header />
      <main className="flex flex-1 w-full flex-col items-center justify-center text-center px-4 mb-[100px] sm:mt-20">
        <h1 className="sm:text-7xl text-4xl max-w-2xl font-bold text-slate-900">
          OpenAI
        </h1>
        <p className="sm:text-xl text-lg max-w-md font-bold text-slate-900 mt-5">
          Using OpenAI's + GPT-3 API
        </p>
        <div className="flex mt-5 mb-[-5px]">
          <button
            className={`px-4 py-2 font-semibold rounded-md ${
              chatGpt ? "bg-black text-white" : "bg-gray-300 text-black"
            }`}
            onClick={() => set_tab("chat_gpt")}
          >
            Chat GPT
          </button>
          <button
            className={`px-4 py-2 font-semibold rounded-md ${
              translate ? "bg-black text-white" : "bg-gray-300 text-black"
            }`}
            onClick={() => set_tab("translate")}
          >
            Translate Text
          </button>
          <button
            className={`px-4 py-2 font-semibold rounded-md ${
              audio ? "bg-black text-white" : "bg-gray-300 text-black"
            }`}
            onClick={() => set_tab("audio")}
          >
            Translate Audio
          </button>
        </div>
        {translate ? <TextTranslate /> : audio ? <AudioTranslate /> : <Chat />}

        <Toaster
          position="top-center"
          reverseOrder={false}
          toastOptions={{ duration: 2000 }}
        />
        <hr className="h-px bg-gray-700 border-1 dark:bg-gray-700" />
      </main>
      <footer className="w-full ">
        <p className="border-t-2 flex justify-center font-semibold py-2">
          ©Rewoke 2023-24
        </p>
      </footer>
    </div>
  );
};

export default Home;
